% Name:- Piyush Malpure
% Exp.:- Slicing of 8bit image
%-----------------------------------------------------------%

clc;
clear all;
close all;
F=imread('cameraman.tif');
subplot(3,3,1);
imshow(F);
G1=bitget(F,1);
subplot(3,3,2);

imshow(logical(G1));
G2=bitget(F,2);
subplot(3,3,3);
imshow(logical(G2));
G3=bitget(F,3);
subplot(3,3,4);
imshow(logical(G3));
G4=bitget(F,4);
subplot(3,3,5);
imshow(logical(G4));
G5=bitget(F,5);
subplot(3,3,6);
imshow(logical(G5));
G6=bitget(F,6);
subplot(3,3,7);
imshow(logical(G6));
G7=bitget(F,7);
subplot(3,3,8);
imshow(logical(G7));
G8=bitget(F,8);
subplot(3,3,9);
imshow(logical(G8));
