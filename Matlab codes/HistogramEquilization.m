% Name:- Piyush Malpure
% Exp.:- Basic Histogram using mathematical tools
%-----------------------------------------------------------%


clc;
clear all;
close all;
I=[4 4 4 4 4;3 4 5 4 3;3 5 5 5 3;3 4 5 4 3;4 4 4 4 4];
[m,n]=size(I);
h=zeros(1,8);
for i=1:m
    for j=1:n
        t=I(i,j)  ;    %represents pixel value
        h(t)=h(t)+1;
    end 
end
bar(h);
no_of_pixels=(m*n);
PDF=zeros(1,8);
for k=1:8
    PDF(k)=h(k)/(no_of_pixels);
end

CDF=zeros(1,8);
for l=1:8
    if(l==1)
        CDF(l)=PDF(l);
    else
        CDF(l)=PDF(l)+CDF(l-1);
    end
end
d=zeros(1,8);
r=zeros(1,8);
for a=1:8
    d(a)=CDF(a)* 7;
    r(a)=round(d(a));
end
heqm = zeros(5,5);
heq=zeros(1,8);
for i=1:m
    for j=1:n
        heqm(i,j)=r(I(i,j));
        tt=heqm(i,j);
        heq(tt)=heq(tt)+1;
    end 
end
bar(heq); 
disp('Equilized image');
disp(heqm);
