% Name:- Piyush Malpure
% Exp.:- Image thresholding
%-----------------------------------------------------%
 
clc;
clear all;
close all;
 
a = imread('Trees.tif');
subplot(2,3,1);
imshow(a);
title('Original image');
[r c] = size(a);
 
%Global thresholding
b = a;
for i = 1:1:r
    for j = 1:1:c
        if (b(i,j) < 127)
            b(i,j) = 0;
        else 
            b(i,j) = 255;
        end
    end
end
 
 
subplot(2,3,2);
imshow(b);
title('Global thresholding of image');
 
%Mean thresholding
tmean = mean(mean(a));
m = a;
for x = 1:r
    for y = 1:c
        if (a(x,y) < tmean)
            m(x,y) = 0;
        else
            m(x,y) = 255;
         end 
    end
end
            
subplot(2,3,3);
imshow(m);
title('Mean thresholding of image');
 
%median thresholding
tmed = median(median(a));
md = a;
for x = 1:r
    for y = 1:c
        if (a(x,y) < tmed)
            md(x,y) = 0;
        else
            md(x,y) = 255;
         end 
    end
end
            
subplot(2,3,4);
imshow(md);
title('Median thresholding of image');
 
% max-min adaptive thresholding
tmax = max(max(a));
tmin = min(min(a));
tmm = (tmax + tmin)/2;
mm = a;
for x = 1:r
    for y = 1:c
        if (a(x,y) < tmm)
            mm(x,y) = 0;
        else
            mm(x,y) = 255;
        end 
    end
end
            
subplot(2,3,5);
imshow(mm);
title('Msx-min adaptive thresholding of image');
 
%adaptive thresholding
add = m + md + mm;
subplot(2,3,6);
imshow(add);
title('Adaptive thresholding of image');
