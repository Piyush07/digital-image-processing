% Name:- Piyush Malpure
% Exp.:- Take a Digital Negative of an Image
%-----------------------------------------------------------%

a=imread('cameraman.tif');
subplot(2,2,1);
imshow(a);
L=256 %total number of pixel values
s=(L-1)-a;
subplot(2,2,2);
imshow(s);

