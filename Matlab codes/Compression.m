% Name:- Piyush Malpure
% Exp.:- Image compression using DCT
%----------------------------------------------------%
 
clc;
clear all;
close all;
 
a = imread('cameraman.tif');
subplot(3,2,1);
imshow(a);
title('Original image');
 
d = dct2(a);
subplot(3,2,2);
imshow(d);
title('DCT of original image');
 
id = idct2(d);
subplot(3,2,3);
imshow(uint8(id));
title('IDCT of original image');
 
d(abs(d) < 30) = 0;
subplot(3,2,4);
imshow(uint8(id));
title('DCT of absolute image');
 
idt = idct2(d);
subplot(3,2,5);
imshow(uint8(id));
