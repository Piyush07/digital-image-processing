% Name:- Piyush Malpure
% Exp.:- Image morphological operations with inbuilt functions
%--------------------------------------------------------------------%
 
clc;
clear all;
close all;
 
a = imread('mandrill.jpg');
a = im2bw(a);  %use only for colored images
subplot(2,3,1);
imshow(a);
title('Original image');
 
% Using functions
SE = [1 1 1;1 1 1;1 1 1];
 
d = imdilate(a,SE);
subplot(2,3,2);
imshow(d);
title('Dilated image');
 
e = imerode(a,SE);
subplot(2,3,3);
imshow(e);
title('Eroded image');
 
c = imclose(a,SE);
subplot(2,3,4);
imshow(c);
title('Closed image');
 
o = imopen(a,SE);
subplot(2,3,5);
imshow(o);
title('Opened image');
