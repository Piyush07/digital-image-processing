% Name:- Piyush Malpure
% Exp.:- Power law transform
%-----------------------------------------------------------%

clc;
clear all;
close all;
F=imread('cameraman.tif')
imshow(F);

title('Orignal Image');
C=1;
D=im2double(F);
[m n]=size(D);
g=[0.5 0.8 1 3 5];
for r=1:length(g)
    for i=1:m
        for j=1:n
            I(i,j)=C*D(i,j).^g(r);
        end
    end
    figure;
imshow(I);
title('Power Law Transform');
xlabel(['Gamma',num2str(g(r))]);
end
