% Name:- Piyush Malpure
% Exp.:- Image restoration
%--------------------------------------------------%
 
clc;
clear all;
close all;
 
a = imread('cameraman.tif');
subplot(2,3,1);
imshow(a);
title('Original image');
 
j1 = imnoise(a,'gaussian');
subplot(2,3,2);
imshow(j1);
title('Gaussian noise added image');
 
j2 = imnoise(a,'salt & pepper');
subplot(2,3,3);
imshow(j2);
title('S&P noise added image');
 
i = fspecial('gaussian');
FJ1 = imfilter(medfilt2(j1,[3 3]),i);
subplot(2,3,4);
imshow(FJ1);
title('Restored image (G)');
 
FJ2 = imfilter(medfilt2(j2,[3 3]),i);
subplot(2,3,5);
imshow(FJ2);
title('Restored image (S&P)');
