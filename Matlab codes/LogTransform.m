% Name:- Piyush Malpure
% Exp.:- Perform Log transform on 8 bit image
%-----------------------------------------------------------%

clc;
clear all;
close all;
F=imread('cameraman.tif');
subplot(1,2,1);
imshow(F);
C=255/(log10(256));
NF=C*log10(1+double(F));
subplot(1,2,2);
imshow(uint8(NF));
