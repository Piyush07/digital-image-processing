% Name:- Piyush Malpure
% Exp.:- Prewitt's operator for image segmentation
%--------------------------------------------------------------------------%

clc;
clear all;
close all;

I = imread('cameraman.tif');
F = double(I);
[r c] = size(F);

p1 = [-1 0 1;-1 0 1;-1 0 1];
p2 = [-1 -1 -1;0 0 0;1 1 1];

for x=2:1:r-1
    for y=2:1:c-1
        h1(x,y) = 0;
        h2(x,y) = 0;
        for i=-1:1
            for j=-1:1
                h1(x,y) = h1(x,y) + p1(i+2,j+2)* F(x+i,y+j);
                h2(x,y) = h2(x,y) + p2(i+2,j+2)* F(x+i,y+j);
            end
        end
    end
end

h3 = h1 + h2;

figure(1);
subplot(2,2,1);
imshow(uint8(I));
title('Original image');

subplot(2,2,2);
imshow(uint8(h1));
title('x - gradient image using Prewitts mask');

subplot(2,2,3);
imshow(uint8(h2));
title('y - gradient image using Prewitts mask');

subplot(2,2,4);
imshow(uint8(h3));
title('Final gradient image');
                
                