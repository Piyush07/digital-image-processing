% Name:- Piyush Malpure
% Exp.:- Image morphological operations using formulae
%--------------------------------------------------------------%
 
clc;
clear all;
close all;
 
a = imread('mandrill.jpg');
a = im2bw(a);  %use only for colored images
subplot(2,3,1);
imshow(a);
title('Original image');
 

% Using formulae

SE = [1 1 1;1 1 1;1 1 1];
[r c] = size(a);
 
p1 = zeros(3,3);
p2 = zeros(3,3);
 
for x=2:r-1
    for y=2:c-1
        p = [a(x-1,y-1)*SE(1) a(x,y-1)*SE(2) a(x+1,y-1)*SE(3) a(x-1,y)*SE(4) a(x,y)*SE(5) a(x+1,y)*SE(6) a(x-1,y+1)*SE(7) a(x,y+1)*SE(8) a(x+1,y+1)*SE(9)];
        p1(x,y) = max(p);
        p2(x,y) = min(p);
    end
end
 
p3 = zeros(3,3);
[r c] = size(p1);
 
for x=2:r-1
    for y=2:c-1
        pq = [p1(x-1,y-1)*SE(1) p1(x,y-1)*SE(2) p1(x+1,y-1)*SE(3) p1(x-1,y)*SE(4) p1(x,y)*SE(5) p1(x+1,y)*SE(6) p1(x-1,y+1)*SE(7) p1(x,y+1)*SE(8) p1(x+1,y+1)*SE(9)];
        p3(x,y) = min(pq);
    end
end
 
p4 = zeros(3,3);
[r c] = size(p2);
 
for x=2:r-1
    for y=2:c-1
        pqq = [p2(x-1,y-1)*SE(1) p2(x,y-1)*SE(2) p2(x+1,y-1)*SE(3) p2(x-1,y)*SE(4) p2(x,y)*SE(5) p2(x+1,y)*SE(6) p2(x-1,y+1)*SE(7) p2(x,y+1)*SE(8) p2(x+1,y+1)*SE(9)];
        p4(x,y) = max(pqq);
    end
end
 
subplot(2,3,2);
imshow(p1);
title('Dilated image');
 
subplot(2,3,3);
imshow(p2);
title('Eroded image');
 
subplot(2,3,4);
imshow(p3);
title('Closed image');
 
subplot(2,3,5);
imshow(p4);
title('Opened image');
