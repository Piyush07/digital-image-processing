% Name:- Piyush Malpure
% Exp.:- Robert's Mask for image segmentation
%--------------------------------------------------------------------------%

clc;
clear all;
close all;

a = imread('cameraman.tif');
a = im2double(a);
J = zeros(size(a));
K = J;
L = J;
Hx = [+1 -1;0 0];
Hy = [1 0;-1 0];
Hxy = Hx + Hy;
[r c] = size(a);
for i = 1 : r-1 
    for j = 1 : c-1
        g1 = zeros(2,2);
        g2 = zeros(2,2);
        g3 = zeros(2,2);
        for k = 1 : 2
            for l = 1 : 2
                g1(k,l) = Hx(3-k,3-l).*a(i+k-1,j+l-1);
                J(i,j) = sum(sum(g1));
                g2(k,l) = Hy(3-k,3-l).*a(i+k-1,j+l-1);
                K(i,j) = sum(sum(g2));
                g3(k,l) = Hxy(3-k,3-l).*a(i+k-1,j+l-1);
                L(i,j) = sum(sum(g3));
            end
        end
    end
end
figure(1);
subplot(2,2,1);
imshow(a);
title('Original image');


subplot(2,2,2);
imshow(J);
title('Roberts mask Hx =[1 0;0 -1]');


subplot(2,2,3);
imshow(K);
title('Roberts mask Hy =[1 0;-1 0]');

subplot(2,2,4);
imshow(L);
title('Roberts mask Hxy = Hx + Hy');
                
        