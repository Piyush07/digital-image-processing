% Name:- Piyush Malpure
% Exp.:- Filters in frequency domain:- Butterworth HPF
%-----------------------------------------------------------%
 
clc;
clear all;
close all;
 
a = imread('cameraman.tif');
a = double(a);
[r c] = size(a);
M = r;
N = c;
h = zeros(r,c);
 
n = input('Enter the order of the filter');
D0 = input('Enter the cutoff frequency of the filter');
 
tic
for u = 1:1:r
    for v = 1:1:c
        D = ((u-M/2)^2 + (v-N/2)^2)^0.5;
        h(u,v) = 1-(1/(1+((D/D0)^(2^n))));
       
    end
end
toc
 
vv = fft2(a);
vc = fftshift(vv);
x = vc.*h;
xx = abs(ifft2(x));
 
 
subplot(2,2,1);
imshow(uint8(a));
title('Original image');
 
 
subplot(2,2,2);
imagesc(h),colormap(gray);
 
 
subplot(2,2,3);
mesh(h);
 
subplot(2,2,4);
imshow(uint8(xx));
